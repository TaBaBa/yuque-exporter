#!/usr/bin/env node
import inquirer from 'inquirer'
import { exit } from 'process';

inquirer
    // 可支持多个输入
    .prompt([{
        type: 'input',          // 类型
        name: 'ACCESSURL',       // 字段名称，在then里可以打印出来
        message: '请输入个人文档访问路径:',  // 提示信息
        // default: 'noname',      // 默认值
        validate: function (v) {// 校验：当输入的值为是string类型，才能按回车，否则按了回车并无效果
            return (typeof v === 'string')&&(v.trim()!='')
        },
        // transformer: function (v) {// 提示信息（输入的信息后缀添加(input your name)）
        //     return v + '(input your name)'
        // },
        // filter: function (v) {// 最终结果
        //     return 'name['+v+']'
        // }
    }, {
        type: 'input',
        name: 'USER',
        message: '请输入语雀账号：',
        validate: function (v) {
            return (typeof v === 'string')&&(v.trim()!='')
        },
    }, {
        type: 'password',
        name: 'PASSWORD',
        message: '请输入语雀密码：',
        validate: function (v) {
            return (typeof v === 'string')&&(v.trim()!='')
        },
    }, {
        type: 'input',
        name: 'EXPORT_PATH',
        message: '请输入导出的文档的存放位置：',
        validate: function (v) {
            return (typeof v === 'string')&&(v.trim()!='')
        },
    }, {
        type: 'list',
        name: 'isDownloadImg',
        message: '是否下载md文档的图片并替换md文档的地址路径：',
        default: false,
        choices: [
            { value: false, name: '否' },
            { value: true, name: '是' }
            
        ]
    }, {
        type: 'input',
        name: 'chromePath',
        message: '浏览器执行文件的路径（可不填）：',
        
    }])
    .then(answers => {
        // 取值
        // console.log('answers', answers.ACCESSURL,answers.USER,answers.PASSWORD,answers.EXPORT_PATH,answers.isDownloadImg)
        import('./downloadMd.js').then(async res=>{
            await res.default(answers.ACCESSURL,answers.USER,answers.PASSWORD,answers.EXPORT_PATH,answers.chromePath)
            if(answers.isDownloadImg){
                import('./downloadImage.js').then(async res=>{
                    await res.default(answers.EXPORT_PATH)
                    exit(0)
                })
            }
        })
    })
    .catch(error => {
        exit(1)
    });
