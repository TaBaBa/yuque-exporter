import puppeteer from 'puppeteer';
import fs from 'fs';
import JSONStream from 'jsonstream';
import { Readable } from 'stream';
import path from 'path';
import { win32 } from "node:path";

class BookPage {
  constructor(id, name, slug) {
    this.id = id;
    this.name = name;
    this.slug = slug
  }
}
class Book {
  pages
  pageLength
  constructor(id, name, slug) {
    this.id = id;
    this.name = name;
    this.slug = slug
  }
}

const downloadMd = async (ACCESSURL, USER, PASSWORD, EXPORT_PATH, chromePath) => {
  
  // const page = await BrowserPage.getInstance();
  const browser = await puppeteer.launch({ headless: false,executablePath:chromePath?chromePath:undefined}); // true:not show browser
  const page = await browser.newPage();
  // 先登录
  console.log("Login use user + password...")
  await page.goto('https://www.yuque.com/login');
  // Switch to password login
  await page.click('.switch-btn');
  // Fill in phone number and password
  await page.type('input[data-testid=prefix-phone-input]', USER);
  await page.type('input[data-testid=loginPasswordInput]', PASSWORD);
  // Check agreement checkbox
  await page.click('input[data-testid=protocolCheckBox]');
  // Click login button
  await page.click('button[data-testid=btnLogin]');

  // 等待页面跳转完成
  await page.waitForNavigation();
  console.log("Login successfully!")
  console.log("Get book stacks ...")
  var books = [];
  const response = await page.goto('https://www.yuque.com/api/mine/book_stacks', { waitUntil: 'networkidle0' });
  const bookListData = await response.text();
  const stream = new Readable({
    read() {
      this.push(bookListData);
      this.push(null); // stream end
    }
  });
  const parser = JSONStream.parse('data.*');
  stream.pipe(parser);
  parser.on('data', function(object) {
    for ( let i = 0; i < object.books.length; i++ ) {
      books.push(new Book(object.books[i].id, object.books[i].name, object.books[i].slug))
    }
  });
  return new Promise(resolve => {
    parser.on('end', async () => {
      console.log(`Books count is: ${books.length}`)
      var bookPages = [];
      for ( let i = 0; i < books.length; i++ ) {
        bookPages[i] = [];
        var bookUrl = 'https://www.yuque.com/api/docs?book_id=' + books[i].id
        var bookResponse = await page.goto(bookUrl, { waitUntil: 'networkidle0' });
        var pageListData = await bookResponse.text();
        var bookStream = new Readable({
          read() {
            this.push(pageListData);
            this.push(null); // stream end
          }
        });
        var bookParser = JSONStream.parse('data.*');
        bookStream.pipe(bookParser);
        bookParser.on('data', (object) => {
          bookPages[i].push(new BookPage(object.id, object.title, object.slug))
        });

        bookParser.on('end', () => {
          console.log(`No.${i+1} Book's name: ${books[i].name}`)
          console.log(bookPages[i])
          console.log()
          books[i].pages = bookPages[i]
          books[i].pageLength = bookPages[i].length
          
          if (i === books.length - 1) {
            resolve();
          }
        });
      }
    });
  }).then(async () => {
    console.log("Start export all books ...")
    const folderPath = EXPORT_PATH;
    console.log("download folderPath: " + folderPath)
    if (!fs.existsSync(folderPath)) {
      console.error(`export path:${folderPath} is not exist`)
      process.exit(1)
    }
    const client = await page.target().createCDPSession()
    await client.send('Page.setDownloadBehavior', {
      behavior: 'allow',
      downloadPath: win32.resolve(win32.normalize(folderPath)), // 此处一个巨大的坑  不能直接填写路径
      // downloadPath: folderPath,
    })
    for ( let i = 0; i < books.length; i++ ) {
      for (let j = 0; j < books[i].pages.length; j++ ) {
        await downloadMardown(page, folderPath, books[i].name, books[i].pages[j].name.replace(/\//g, '_') , ACCESSURL + "/" + books[i].slug + "/" + books[i].pages[j].slug)
        console.log()
      }
    }
    fs.readdir(folderPath, (err, files) => {
      if (err) throw err;
      files.forEach((file) => {
        const filePath = path.join(folderPath, file);
        fs.stat(filePath, (err, stat) => {
          if (err) throw err;
          if (stat.isFile()) {    
            fs.unlink(filePath, (err) => {
              if (err) throw err;
            });
          }
        });
      });
      
      console.log()
      console.log(`Clean useless files successfully`);
      console.log()
      console.log(`Export successfully! Have a good day!`);
      console.log()
    });
  }).finally(()=>{
    browser.close()
  });
  // browserpage, bookName, url
  async function downloadMardown(page, rootPath, book, mdname, docUrl) {
    const url = 'https://www.yuque.com/' + docUrl + '/markdown?attachment=true&latexcode=false&anchor=false&linebreak=false';
    const timeout = 10000; // 10s timeout
    const maxRetries = 3; // max retry count
    const newPath = path.join(rootPath, book);
    

    async function goto(page, link) {
      return page.evaluate((link) => {
          location.href = link;
      }, link);
    }
    async function waitForDownload(mdname, started = false) {
      return new Promise((resolve, reject) => {
        const watcher = fs.watch(rootPath, (eventType, filename) => {
          // console.log(`watch ${eventType} ${filename}, want ${mdname}.md`)
          if (eventType === 'rename' && filename === `${mdname}.md.crdownload` && !started) {
            console.log("Downloading document " + book + "/" + mdname)
            started = true
          }

          if (eventType === 'rename' && filename === `${mdname}.md` && started) {
            watcher.close();
            resolve(filename);
          }
        });

        setTimeout(() => {
          watcher.close();
          reject(new Error('Download timed out'));
        }, timeout);
      });
    }
    async function downloadFile(mdname, url, recount = 0, retries = 0) {
      var count = recount
      try {
        const fileNameWithExt = await waitForDownload(mdname);
        const oldFiles = path.join(rootPath, fileNameWithExt);
        const fileName = path.basename((fileNameWithExt), path.extname(fileNameWithExt));
        console.log("Download document " + book + "/" + fileName + " finished")
        var newFiles = path.join(newPath, fileName.replace(/\//g, '_') + '.md');
        while (fs.existsSync(newFiles)) {
          count++;
          newFiles = path.join(newPath, fileName.replace(/\//g, '_') + `(${count}).md`);
        }

        fs.renameSync(oldFiles, newFiles);
        console.log('Moved file to:', newFiles);
      } catch (error) {
        console.log(error);
        if (error.message === 'Download timed out' && retries < maxRetries) {
          console.log(`Retrying download... (attempt ${retries + 1})`);
          await goto(page, url);
          await downloadFile(mdname, url, count, retries + 1);
        } else {
          console.log(`Download error: ` + error);
        }
      }
    }

    if (!fs.existsSync(newPath)) {
      fs.mkdirSync(newPath)
    }
    // console.log(book + "/" + mdname + "'s download URL is: " + url)
    await goto(page, url);
    await downloadFile(mdname, url)
  }
};




export default downloadMd
