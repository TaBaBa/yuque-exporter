import * as fs from 'node:fs/promises';
import path from 'path';
import remarkParse from 'remark-parse'
import remarkStringify from 'remark-stringify'
import {unified} from 'unified'
import {visit} from 'unist-util-visit'
import url from 'url'
import axios from 'axios'

const __dirname = path.resolve()
let imageArr = {}

const imageReplacePlugin = (filePath) => ()=>{
  imageArr[filePath] = []
  return (tree) => {
    visit(tree, 'image', (node) => {
      if(node?.url.startsWith('https://')){
        const imgDirName = withoutExtName(filePath)
        const imageInfo = {
          url: node.url,
          name: path.basename(path.resolve(url.parse(node.url).pathname)),
          path: path.resolve(path.dirname(filePath),`./${imgDirName}`)
        }
        imageArr[filePath].push(imageInfo)
        node.url = `${imgDirName}/${imageInfo.name}`
      }
    });
  };
};
const readeMdFile = async filePath => await fs.readFile(filePath, {encoding:'utf8'});
const parseMd = async (mdstr,filePath) => {
  return await unified()
  .use(remarkParse)
  .use(imageReplacePlugin(filePath))
  .use(remarkStringify)
  .process(mdstr)
}
const withoutExtName = filename => {
  const fileName = path.basename(filename);
  const fileNameWithoutExt = path.parse(fileName).name;
  return fileNameWithoutExt
}
const traverseFiles = async (rootDir) => {
  const files = await fs.readdir(rootDir);
  for (const filename of files) {
    const filePath = path.join(rootDir, filename);
    const stat = await fs.stat(filePath);
    if (stat.isDirectory()) {
      await traverseFiles(filePath)
      continue
    }else{
      const fileNameWithoutExt = withoutExtName(filename)
      // 解析md文件
      if(path.extname(filename).toLowerCase() === '.md'){
        const fileStr = await readeMdFile(filePath)
        const result = await parseMd(fileStr,filePath)
        // 将处理后的md文件替换原有的文件
        await fs.writeFile(filePath,String(result),{encoding:'utf-8'})
      }
    }
  }
}
const downloadImage = async (url, path) => {
  return new Promise(async (resolve,reject)=>{
    try {
      const response = await axios.get(url, { responseType: 'stream' });
      await fs.writeFile(path, response.data);  
      console.log(`${path} saved successfully.`);
      resolve()
    } catch (error) {
      console.error(`${path} downloading image error`);
      reject()
    }
  })  
}; 


export default async (rootDir) => {
  console.log('--------start download images---------')
  await traverseFiles(rootDir)
  let remainImg = {}
  // 下载图片数据
  let keyarr = Object.keys(imageArr)
  for (let i = 0; i < keyarr.length; i++) {
    let key = keyarr[i]
    const arr = imageArr[key]
    for (let i = 0; i < arr.length; i++) {
      console.log(`${key}------${i}`)
      try {
        await fs.mkdir(arr[i].path, { recursive: true });
        await downloadImage(arr[i].url,path.resolve(arr[i].path,arr[i].name)).catch(e=>{
          remainImg[key] = []
          remainImg[key].push(arr[i])
        })
      } catch (error) {
        remainImg[key] = []
        remainImg[key].push(arr[i])
      }
    }
  }
  // 未下载的下载的图片信息保存到文件
  await fs.writeFile(path.join(rootDir,'./remainImg.json'), JSON.stringify(remainImg), {encoding:'utf8'});
  console.log('--------download end---------')
  console.log('--------没有下载下来的图片将会在导出目录下的remainImg.json文件中记录---------')
  
}
