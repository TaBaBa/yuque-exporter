
# 语雀导出文档工具
### 功能：

- 模拟用户浏览器操作一篇一篇导出 markdown 文档
- 支持同名的文档导出
- 支持导出失败重试
- 支持导出md文件的引用图片

我的知识库与导出文件目录展示：
![image.png](https://images.ryken.cloud/2023/05/91804cc3646d6356cd7458c9a12444fc.png)

![image.png](https://images.ryken.cloud/2023/05/4b3a4e4207ead71f15600806c12a5c1d.png)


### 说明：

这是一个基于puppeteer 来模拟用户在浏览器的操作一篇一篇的导出语雀文档的工具。
关于语雀的导出可以详情说明见官方的文档：[如何导入导出知识库](https://www.yuque.com/yuque/thyzgp/import-lake-to-lark) 

首先语雀支持导出文档为 markdown 格式。
- 单篇导出：支持导出为 markdown、word、pdf、lakebook等
- 批量导出：支持导出为 lakebook、pdf 格式。对于超级用户是可以通过创建 token 来使用[官方的 exporter 工具](https://github.com/yuque/yuque-exporter)或者其他基于 api 的工具进行批量导出；超级用户的价格为 299/年。

在这位[大哥](https://github.com/renyunkang/yuque-exporter.git)的基础上,增加了md文件中的图片统一导出和替换的操作

### 使用前提：
- 确保你的环境有 Chromium 浏览器。如 Google Chrome、Microsoft Edge、Opera 和 Brave等，都是基于 Chromium 浏览器构建的。
- 提前准备个人文档访问路径，语雀的账号及密码，导出的文件夹路径（比如：D:/私有仓库/ceshi），最后如有必要也可以修改浏览器的执行路径（比如：C:/Program Files/Google/Chrome/Application/chrome.exe）。


#### 1. 获取个人文档访问路径
访问个人账号管理： https://www.yuque.com/settings/account
![Alt text](images/0000.png)

个人路径：账号设置 -> 账号管理可以查看已经设置的个人路径 `https://www.yuque.com/xxx` **☆☆☆此处的xxx就是个人文档访问路径☆☆☆**

#### 2. 安装 node 相关的工具 node要求在16.14.2以上
建议使用 nvm 管理 node，选取下列适合自己的方式安装：
- github 地址：[nvm-sh/nvm: Node Version Manager](https://github.com/nvm-sh/nvm)
- gitee 地址：[nvm-cn: 🧊 nvm国内安装工具 (gitee.com)](https://gitee.com/RubyKids/nvm-cn)

配置 npm 淘宝源：npm config set registry https://registry.npm.taobao.org


#### 3. 安装并运行工具
在命令行窗口

安装
- `npm install yuque-batch-export -g`

运行
- `yuque`

然后根据提示依次输入必要的信息

注意：最后导出的文档的存放位置请填写完整路径并保证路径的文件夹存在。比如：D:/私有仓库/ceshi


### 存在的问题：
1.无法获取知识库的目录信息，进一步做层级关系的文档导出存储
现在普通用户无法获取知识库中的目录信息，因此知识库文档的导出后全部平铺保存到以知识库名称命名的文件夹中。同名的文档会在文档后面追加数字以区分，知识库目录的分级需要自己根据文档的内容进行调整。

解决办法：访问目录的url为 https://www.yuque.com/个人路径/知识库slug/toc，有兴趣的可以根据 html 的元素的特征来分析。（我个人没有这个需求也就没有太大关注了，如果你有兴趣或者有需求可以试试，类似自动登录时按照元素匹配数据）

2.自动登录仅支持账号密码登录

3.无法保证兼容性，如果之后官方 api 修改后，可以自己根据 api 修改源码

### Q&A
1.  Could not find Chromium 但是有 chorm 浏览器
在不同的操作系统上，Puppeteer 默认调用的 Chrome 路径如下：
-   Windows: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
-   macOS: /Applications/Google Chrome.app/Contents/MacOS/Google Chrome
-   Linux: /usr/bin/google-chrome
如果主机上对应的可执行文件路径与默认一致但仍然运行失败，可以修改源码手动指定一下，如果没有chorme也可以执行edge的二进制文件
![image.png](https://images.ryken.cloud/2023/05/eb093fe57cb0b6cc557a9616f5899445.png)
```js
const browser = await puppeteer.launch({ headless: true });
 to
const browser = await puppeteer.launch({ headless: true, executablePath: '/usr/bin/google-chrome' });
 or
// headless: false 会打开浏览器实时观察模拟的操作，可用于调试；executablePath 替换为自己本机对应路径
const browser = await puppeteer.launch({ headless: false, executablePath: "C:\\Program Files (x86)\\Microsoft\\Edge\\Application\\msedge.exe" });
```
有其他问题可以自己修改源代码
[本项目的仓库地址](https://gitee.com/TaBaBa/yuque-exporter)
